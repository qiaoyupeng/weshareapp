//
//  CommentCell.swift
//  WeshareApp
//
//  Created by qiaoqiao peng on 4/30/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit
import Firebase

class CommentCell: UITableViewCell {

    @IBOutlet weak var postContent: UITextView!
    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var unlikeButton: UIButton!
    
    
    var postID: String!
    

    
    
    
    
    
}
