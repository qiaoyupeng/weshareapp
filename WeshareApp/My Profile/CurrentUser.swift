//
//  CurrentUser.swift
//  WeshareApp
//
//  Created by qiaoqiao peng on 4/25/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import Foundation

struct CurrentUser {

    let name: String!
    let email: String!
    let profileImageUrl: String!
    
    init(uid: String, dictionary: [String: Any]) {
        self.name = dictionary["full name"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.profileImageUrl = dictionary["urlToImage"] as? String ?? ""
    }
}
