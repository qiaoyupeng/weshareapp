//
//  MyProfileViewController.swift
//  WeshareApp
//
//  Created by qiaoqiao peng on 4/25/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit
import Firebase

class MyProfileViewController: UIViewController {

    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    var dataBaseRef: DatabaseReference! {
        
        return Database.database().reference()
    }
    
    var storageRef: Storage! {
        
        return Storage.storage()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        uploadUserInfo()
    }

    
    
    @objc func uploadUserInfo(){
        
        if Auth.auth().currentUser != nil {
            
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            Database.database().reference().child("users").child(uid).observeSingleEvent (of: .value, with: { (snapshot) in
                
                guard let dict = snapshot.value as? [String : Any] else { return }
                
                let user = CurrentUser(uid: uid, dictionary: dict)
                
                self.usernameLabel.text = user.name
                self.emailLabel.text = user.email
                
                self.userImageView.downloadImage(from: user.profileImageUrl)
                
                
            }
        )}
    }
}
