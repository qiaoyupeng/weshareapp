//
//  Post.swift
//  WeshareApp
//
//  Created by qiaoqiao peng on 4/25/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit

class Post: NSObject {
    
    var author: String!
    var likes: Int!
    var pathToImage: String!
    var postContent: String!
    var userID: String!
    var postID: String!
    
    var postComment: String!
    
    var peopleWhoLike: [String] = [String]()

}
