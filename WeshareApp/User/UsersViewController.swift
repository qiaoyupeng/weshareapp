//
//  UsersViewController.swift
//  WeshareApp
//
//  Created by qiaoqiao peng on 4/24/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit
import Firebase

class UsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tabelview: UITableView!
    
    var user = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        retrieveUsers()

        // Do any additional setup after loading the view.
    }

    
    func retrieveUsers(){
        let ref = Database.database().reference()
        
        ref.child("users").queryOrderedByKey().observeSingleEvent(of: .value, with: { snapshot in
            let users = snapshot.value as! [String : AnyObject]
            self.user.removeAll()
            for (_,value) in users{
                if let uid = value["uid"] as? String {
                    if uid != Auth.auth().currentUser!.uid{
                        let userToshow = User()
                        if let fullname = value["full name"] as? String,
                            let imagePath = value["urlToImage"] as? String,
                            let useremail = value["email"] as? String{
                            userToshow.fullName = fullname
                            userToshow.useremail = useremail
                            userToshow.imagePath = imagePath
                            userToshow.userID = uid

                            self.user.append(userToshow)    
                        }
                    }
                }
            }
            self.tabelview.reloadData()
        })
        ref.removeAllObservers()
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabelview.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserCell
        
        cell.nameLabel.text = self.user[indexPath.row].fullName
        cell.userID = self.user[indexPath.row].userID
        cell.emailLabel.text = self.user[indexPath.row].useremail
        cell.userImage.downloadImage(from: self.user[indexPath.row].imagePath!)
        checkFollowing(indexPath: indexPath)
        
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let uid = Auth.auth().currentUser!.uid
        let ref = Database.database().reference()
        let key = ref.child("users").childByAutoId().key
        
        var isFollower = false
        
        ref.child("users").child(uid).child("following").queryOrderedByKey().observeSingleEvent(of: .value, with: {snapshot in
            
            if let following = snapshot.value as? [String : AnyObject] {
                for (ke, value) in following {
                    if value as! String == self.user[indexPath.row].userID {
                        isFollower = true
                        
                        ref.child("users").child(uid).child("following/\(ke)").removeValue()
                        ref.child("users").child(self.user[indexPath.row].userID).child("followers/\(ke)").removeValue()
                        
                        self.tabelview.cellForRow(at: indexPath)?.accessoryType = .none
                    }
                }
            }
            //Follow as user has no followers
            if !isFollower {
                let following = ["following/\(key)" : self.user[indexPath.row].userID]
                let followers = ["followers/\(key)" : uid]
                
                ref.child("users").child(uid).updateChildValues(following)
                ref.child("users").child(self.user[indexPath.row].userID).updateChildValues(followers)
                
                self.tabelview.cellForRow(at: indexPath)?.accessoryType = .checkmark
            }
            
            
            })
       ref.removeAllObservers()
        
    }
 
    
    func checkFollowing(indexPath: IndexPath) {
        let uid = Auth.auth().currentUser!.uid
        let ref = Database.database().reference()
        
        ref.child("users").child(uid).child("following").queryOrderedByKey().observeSingleEvent(of: .value, with: {snapshot in
            
            if let following = snapshot.value as? [String : AnyObject] {
                for (_, value) in following {
                    if value as! String == self.user[indexPath.row].userID{
                        self.tabelview.cellForRow(at: indexPath)?.accessoryType = .checkmark
                    }
                    
                }
            }
        })
        ref.removeAllObservers()
    }
 
}

    
    
extension UIImageView {
    
    func downloadImage(from imgURL: String) {
        let url = URLRequest(url: URL(string: imgURL)!)
        
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            
            if error != nil{
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }
        task.resume()
    }
}

