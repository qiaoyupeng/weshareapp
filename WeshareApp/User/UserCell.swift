//
//  UserCell.swift
//  WeshareApp
//
//  Created by qiaoqiao peng on 4/24/18.
//  Copyright © 2018 LTU. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    var userID: String!
    
}
